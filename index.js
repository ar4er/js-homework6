// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Это способ использования специальных символов как обычных

// Які засоби оголошення функцій ви знаєте?
// function example() {} - обьявление функции. Может вызываться до обьявления.
// let a = function() {} - функциональное выражение. Может вызываться только после обьявления.
// let a = () => {} - тоже, что и функциональное выражение.

// Що таке hoisting, як він працює для змінних та функцій?
// Это процесс поднятия функции или переменной в самый верх своей области видимости.
// Работает только с var (эффект всплытия). Обратиться к let или const до обьявления НЕЛЬЗЯ.
// Вызывать функцию можно где-угодно, если она обьявлена.

function createNewUser(
  userFirstName = prompt("Enter your first name"),
  userLastName = prompt("Enter our last name"),
  userBirthday = prompt("Enter your birthday like dd.mm.yyyy")
) {
  this._firstName = userFirstName;
  this._lastName = userLastName;
  this._birthday = new Date(
    userBirthday.slice(6, 10),
    userBirthday.slice(3, 5) - 1,
    userBirthday.slice(0, 2)
  );

  this.getBirthday = function () {
    return this._birthday.toLocaleDateString();
  };

  this.getPassword = function () {
    return (
      this._firstName.charAt(0).toUpperCase() +
      this._lastName.toLowerCase() +
      this._birthday.getFullYear()
    );
  };

  this.getAge = function () {
    return (
      Math.floor(
        (new Date().getTime() - new Date(this._birthday)) /
          (24 * 3600 * 365.25 * 1000)
      ) + " years"
    );
  };

  this.getLogin = function () {
    return (this._firstName.charAt(0) + this._lastName).toLowerCase();
  };

  Object.defineProperty(this, "setFirstName", {
    set: function (value) {
      this._firstName = value;
    },
  }),
    Object.defineProperty(this, "setLastName", {
      set: function (value) {
        this._lastName = value;
      },
    });
}

let newUser = new createNewUser();

console.log("New user");
console.log(newUser);
console.log("========================");

console.log("getLogin function");
console.log(newUser.getLogin());
console.log("========================");

console.log("getBirthday function");
console.log(newUser.getBirthday());
console.log("========================");

console.log("getAge function");
console.log(newUser.getAge());
console.log("========================");

console.log("getPassword function");
console.log(newUser.getPassword());
console.log("========================");
